import { useEffect, useRef, useState } from 'react';
import './App.scss'; 
import Button from "./components/Button"; 
import Modal from "./components/Modal";
import ProductCard from './components/productCard/ProductCard';
import Header from './components/header/Header';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Cart from './pages/cart/Cart';
import Home from './pages/home/Home';
import Favorites from './pages/favorites/Favorites';

function App() {
    // Створюємо стан для числа вибраних користувачем товарів, яке потім передаватимемо
    // в компонент Header у вигляді пропсів. Аналогічно діємо з доданими в кошик товарами
    const [selectedProductsCount, setSelectedProductsCount] = useState(0);
    const [addedToCartProductsCount, setAddedToCartProductsCount] = useState(0);

    useEffect(() => {
        const selectedProducts = JSON.parse(localStorage.getItem('selectedProducts')) || [];
        const addedToCartProducts = JSON.parse(localStorage.getItem('addedToCartProducts')) || [];
        setSelectedProductsCount(selectedProducts.length);
        setAddedToCartProductsCount(addedToCartProducts.length);
    }, []);

    // Функція для оновлення кількості обраних товарів, що визиватиметься компонентом
    // ProductCard після кожного кліку по starIcon
    function updateSelectedProductsCount() {
        const selectedProducts = JSON.parse(localStorage.getItem('selectedProducts'));
        setSelectedProductsCount(selectedProducts.length);
    }

    // Функція для оновлення кількості доданих у кошик товарів
    function updateAddedToCartProductsCount() {
        const addedToCartProducts = JSON.parse(localStorage.getItem('addedToCartProducts'));
        setAddedToCartProductsCount(addedToCartProducts.length);
    }

    return (
        <div className='App' >
            <Header selectedProductsCount={selectedProductsCount} 
                    addedToCartProductsCount={addedToCartProductsCount}
            />

            <Routes>
                <Route path='/cart' element={<Cart
                                            updateAddedToCartProductsCount={updateAddedToCartProductsCount}
                                        />}/>
                <Route path='/favorites' element={<Favorites
                                            updateSelectedProductsCount={updateSelectedProductsCount}
                                        />}/>
                <Route path='/' element={<Home 
                                            updateSelectedProductsCount={updateSelectedProductsCount}
                                            updateAddedToCartProductsCount={updateAddedToCartProductsCount}
                                        />}/>
                <Route path='/*' element={<Home 
                                            updateSelectedProductsCount={updateSelectedProductsCount}
                                            updateAddedToCartProductsCount={updateAddedToCartProductsCount}
                                        />}/>
            </Routes>

        </div>
    );
}

export default App;
